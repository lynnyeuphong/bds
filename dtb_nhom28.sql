-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th6 20, 2019 lúc 10:09 PM
-- Phiên bản máy phục vụ: 10.1.40-MariaDB
-- Phiên bản PHP: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `bds_hathanh`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tblslideshow`
--

CREATE TABLE `tblslideshow` (
  `id` int(11) NOT NULL,
  `anh` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tblslideshow`
--

INSERT INTO `tblslideshow` (`id`, `anh`) VALUES
(1, 'teaser-slide.png'),
(2, 'banner-1.png');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_baiviet`
--

CREATE TABLE `tbl_baiviet` (
  `id` int(11) NOT NULL,
  `tieude` text COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  `anh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ngaythang` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `trichdan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tenduan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_baiviet`
--

INSERT INTO `tbl_baiviet` (`id`, `tieude`, `noidung`, `anh`, `ngaythang`, `trichdan`, `tenduan`) VALUES
(2, 'VINHOMES CÃ”NG Bá» QUY HOáº CH Láº I CÃC DÃ’NG Sáº¢N PHáº¨M, Äáº¨Y Máº NH PHÃT TRIá»‚N CÃC Äáº I ÄÃ” THá»Š Äáº²NG Cáº¤P QUá»C Táº¾ VÃ€ CÃC KHU NHÃ€ á»ž DÃ€NH CHO NGÆ¯á»œI THU NHáº¬P THáº¤P', '&lt;p&gt;Cá»¥ thá»ƒ, C&amp;ocirc;ng ty sáº½ táº­p trung ph&amp;aacute;t triá»ƒn hai thÆ°Æ¡ng hiá»‡u l&amp;agrave; Vinhomes v&amp;agrave; Happy Town. Trong Ä‘&amp;oacute;, Vinhomes l&amp;agrave; thÆ°Æ¡ng hiá»‡u báº¥t Ä‘á»™ng sáº£n trung v&amp;agrave; cao cáº¥p theo m&amp;ocirc; h&amp;igrave;nh Ä‘áº³ng cáº¥p quá»‘c táº¿, Ä‘&amp;aacute;p á»©ng Ä‘áº§y Ä‘á»§ c&amp;aacute;c ti&amp;ecirc;u chuáº©n sá»‘ng tÆ°Æ¡ng Ä‘Æ°Æ¡ng c&amp;aacute;c nÆ°á»›c ph&amp;aacute;t triá»ƒn; Happy Town l&amp;agrave; thÆ°Æ¡ng hiá»‡u báº¥t Ä‘á»™ng sáº£n Nh&amp;agrave; cho ngÆ°á»i c&amp;oacute; thu nháº­p tháº¥p, nháº±m giáº£i quyáº¿t váº¥n Ä‘á» nh&amp;agrave; á»Ÿ cáº¥p thiáº¿t cho c&amp;ocirc;ng nh&amp;acirc;n c&amp;aacute;c khu c&amp;ocirc;ng nghiá»‡p v&amp;agrave; ngÆ°á»i d&amp;acirc;n thu nháº­p tháº¥p.&lt;/p&gt;\r\n\r\n&lt;p&gt;Äá»“ng thá»i, Vinhomes cÅ©ng tiáº¿n h&amp;agrave;nh chuyá»ƒn Ä‘á»•i c&amp;aacute;c dá»± &amp;aacute;n VinCity th&amp;agrave;nh c&amp;aacute;c Ä‘áº¡i Ä‘&amp;ocirc; thá»‹ mang thÆ°Æ¡ng hiá»‡u Vinhomes. Vá»›i quy m&amp;ocirc; v&amp;agrave; háº¡ táº§ng Ä‘á»“ng bá»™ Ä‘ang Ä‘Æ°á»£c Ä‘áº§u tÆ° theo m&amp;ocirc; h&amp;igrave;nh &amp;ldquo;Singapore v&amp;agrave; hÆ¡n tháº¿ ná»¯a&amp;rdquo;, VinCity há»™i tá»¥ Ä‘áº§y Ä‘á»§ Ä‘iá»u kiá»‡n Ä‘á»ƒ chuyá»ƒn Ä‘á»•i th&amp;agrave;nh ti&amp;ecirc;u chuáº©n Vinhomes vá»›i 3 d&amp;ograve;ng sáº£n pháº©m Ä‘a dáº¡ng, Ä‘&amp;aacute;p á»©ng nhu cáº§u cá»§a kh&amp;aacute;ch h&amp;agrave;ng gá»“m: &lt;strong&gt;Vinhomes Sapphire, Vinhomes Ruby, Vinhomes Diamond. &lt;/strong&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;Trong Ä‘&amp;oacute;, d&amp;ograve;ng sáº£n pháº©m Ä‘áº§u ti&amp;ecirc;n &lt;strong&gt;Vinhomes Sapphire&lt;/strong&gt; l&amp;agrave; d&amp;ograve;ng cÄƒn há»™ hiá»‡n Ä‘áº¡i, d&amp;agrave;nh cho giá»›i tráº» nÄƒng Ä‘á»™ng, Æ°a th&amp;iacute;ch c&amp;ocirc;ng nghá»‡, c&amp;oacute; xu hÆ°á»›ng lá»±a chá»n c&amp;aacute;c giáº£i ph&amp;aacute;p th&amp;ocirc;ng minh v&amp;agrave; linh hoáº¡t trong cuá»™c sá»‘ng. D&amp;ograve;ng sáº£n pháº©m VinCity hiá»‡n nay sáº½ Ä‘Æ°á»£c Ä‘á»•i t&amp;ecirc;n th&amp;agrave;nh Vinhomes Sapphire.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/2019/March/VH%20ruby.jpg&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;em&gt;Vá»›i quy m&amp;ocirc; v&amp;agrave; háº¡ táº§ng Ä‘á»“ng bá»™ Ä‘ang Ä‘Æ°á»£c Ä‘áº§u tÆ° theo m&amp;ocirc; h&amp;igrave;nh &amp;ldquo;Singapore v&amp;agrave; hÆ¡n tháº¿ ná»¯a&amp;rdquo;, VinCity há»™i tá»¥ Ä‘áº§y Ä‘á»§ Ä‘iá»u kiá»‡n Ä‘á»ƒ chuyá»ƒn Ä‘á»•i th&amp;agrave;nh ti&amp;ecirc;u chuáº©n Vinhomes vá»›i 3 d&amp;ograve;ng sáº£n pháº©m l&amp;agrave; Vinhomes Sapphire, Vinhomes Ruby v&amp;agrave; Vinhomes Diamond.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;D&amp;ograve;ng sáº£n pháº©m thá»© hai -&lt;strong&gt; Vinhomes Ruby&lt;/strong&gt; l&amp;agrave; d&amp;ograve;ng cÄƒn há»™ cao cáº¥p hÆ¡n, d&amp;agrave;nh cho nh&amp;oacute;m kh&amp;aacute;ch h&amp;agrave;ng gia Ä‘&amp;igrave;nh vÄƒn minh, hiá»‡n Ä‘áº¡i, hÆ°á»›ng tá»›i cuá»™c sá»‘ng Ä‘áº³ng cáº¥p, Ä‘á»§ Ä‘áº§y dá»‹ch vá»¥ tiá»‡n &amp;iacute;ch. Vinhomes Ruby ch&amp;iacute;nh l&amp;agrave; c&amp;aacute;c sáº£n pháº©m Ä‘áº¡i tr&amp;agrave; cá»§a Vinhomes hiá»‡n nay.&lt;/p&gt;\r\n\r\n&lt;p&gt;D&amp;ograve;ng sáº£n pháº©m thá»© ba &amp;ndash; &lt;strong&gt;Vinhomes Diamond&lt;/strong&gt; l&amp;agrave; d&amp;ograve;ng cÄƒn há»™ cao cáº¥p nháº¥t trong há»‡ thá»‘ng Vinhomes vá»›i c&amp;aacute;c cÄƒn há»™ Ä‘áº³ng cáº¥p, Ä‘áº§y Ä‘á»§ tiá»‡n &amp;iacute;ch cao cáº¥p d&amp;agrave;nh cho c&amp;aacute;c kh&amp;aacute;ch h&amp;agrave;ng tinh hoa.&amp;nbsp;C&amp;aacute;c sáº£n pháº©m cá»§a Vinhomes Diamond sáº½ tÆ°Æ¡ng tá»± nhÆ° c&amp;aacute;c cÄƒn há»™ cá»§a dá»± &amp;aacute;n Vinhomes Golden River.&lt;/p&gt;\r\n\r\n&lt;p&gt;Sau quy hoáº¡ch, c&amp;aacute;c Ä‘áº¡i Ä‘&amp;ocirc; thá»‹ quy m&amp;ocirc; lá»›n sáº½ mang t&amp;ecirc;n Vinhomes v&amp;agrave; c&amp;oacute; Ä‘áº§y Ä‘á»§ cáº£ 3 d&amp;ograve;ng sáº£n pháº©m tr&amp;ecirc;n. C&amp;aacute;c khu Ä‘&amp;ocirc; thá»‹ quy m&amp;ocirc; nhá» hÆ¡n, tuá»³ theo ti&amp;ecirc;u ch&amp;iacute; tÆ°Æ¡ng á»©ng sáº½ thuá»™c thÆ°Æ¡ng hiá»‡u Vinhomes Sapphire, Vinhomes Ruby hoáº·c Vinhomes Diamond.&lt;/p&gt;\r\n\r\n&lt;p&gt;Äáº·c biá»‡t, vá»›i tinh tháº§n ná»— lá»±c kh&amp;ocirc;ng ngá»«ng &amp;ldquo;V&amp;igrave; má»™t cuá»™c sá»‘ng tá»‘t Ä‘áº¹p hÆ¡n cho ngÆ°á»i Viá»‡t&amp;rdquo;, Vinhomes sáº½ tiáº¿p tá»¥c ph&amp;aacute; vá»¡ giá»›i háº¡n cá»§a m&amp;ocirc; h&amp;igrave;nh Ä‘&amp;ocirc; thá»‹ phá»©c há»£p hiá»‡n táº¡i, hÆ°á»›ng Ä‘áº¿n nhá»¯ng Ä‘á»‰nh cao má»›i. C&amp;ugrave;ng vá»›i cÆ¡ sá»Ÿ háº¡ táº§ng, cáº£nh quan tiá»‡n &amp;iacute;ch Ä‘á»“ng bá»™, hiá»‡n Ä‘áº¡i &amp;ndash; c&amp;aacute;c khu Ä‘&amp;ocirc; thá»‹ v&amp;agrave; Ä‘áº¡i Ä‘&amp;ocirc; thá»‹ Vinhomes sáº¯p tá»›i sáº½ Ä‘Æ°á»£c nghi&amp;ecirc;n cá»©u, á»©ng dá»¥ng c&amp;ocirc;ng nghá»‡ th&amp;ocirc;ng minh v&amp;agrave; ká»¹ thuáº­t cao trong quáº£n l&amp;yacute; váº­n h&amp;agrave;nh, mang láº¡i m&amp;ocirc;i trÆ°á»ng sá»‘ng tiá»‡n nghi, Ä‘áº³ng cáº¥p quá»‘c táº¿ cho kh&amp;aacute;ch h&amp;agrave;ng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Song song vá»›i c&amp;aacute;c khu Ä‘&amp;ocirc; thá»‹ Vinhomes, Vinhomes sáº½ Ä‘áº©y máº¡nh viá»‡c ph&amp;aacute;t triá»ƒn c&amp;aacute;c khu Ä‘&amp;ocirc; thá»‹ Happy Town, l&amp;agrave; d&amp;ograve;ng nh&amp;agrave; á»Ÿ d&amp;agrave;nh cho ngÆ°á»i thu nháº­p tháº¥p./&lt;/p&gt;\r\n', 'Screenshot_2.png', '2019/06/21 00:26:24', 'Náº±m trong chiáº¿n lÆ°á»£c ph&aacute;t triá»ƒn c&aacute;c báº¥t Ä‘á»™ng sáº£n Ä‘áº³ng cáº¥p quá»‘c táº¿, kh&ocirc;ng ngá»«ng n&acirc;ng táº§m cháº¥t lÆ°á»£ng cuá»™c sá»‘ng cho ngÆ°á»i d&acirc;n Viá»‡t Nam, C&ocirc;ng ty CP Vinhomes ch&iacute;nh thá»©c c', 'VINHOMES '),
(5, 'VINHOMES RIVERSIDE ÄÆ¯á»¢C VINH DANH LÃ€ â€œBáº¤T Äá»˜NG Sáº¢N Tá»T NHáº¤T THáº¾ GIá»šIâ€ NÄ‚M 2018', '&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;strong&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/2018/Vinhomes/IPA_Anh%201.JPG&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;/strong&gt;&lt;br /&gt;\r\n&lt;em&gt;Äáº¡i diá»‡n Vinhomes nháº­n giáº£i thÆ°á»Ÿng &amp;ldquo;Báº¥t Ä‘á»™ng sáº£n tá»‘t nháº¥t tháº¿ giá»›i&amp;rdquo; 2018 do IPA trao táº·ng.&amp;nbsp;&lt;/em&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;VÆ°á»£t qua nhiá»u v&amp;ograve;ng Ä‘&amp;aacute;nh gi&amp;aacute; nghi&amp;ecirc;m ngáº·t cá»§a Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o quá»‘c táº¿ International Property Awards (IPA), Ä‘áº¡i diá»‡n cá»§a Viá»‡t Nam - Vinhomes Riverside do Táº­p Ä‘o&amp;agrave;n Vingroup Ä‘áº§u tÆ° Ä‘&amp;atilde; vinh dá»± Ä‘á»©ng Ä‘áº§u háº¡ng má»¥c &amp;ldquo;Báº¥t Ä‘á»™ng sáº£n tá»‘t nháº¥t tháº¿ giá»›i&amp;rdquo; (World&amp;rsquo;s Best Property) nÄƒm 2018. ÄÆ°á»£c coi l&amp;agrave; giáº£i thÆ°á»Ÿng cao nháº¥t cá»§a há»‡ thá»‘ng giáº£i thÆ°á»Ÿng IPA, ng&amp;ocirc;i vá»‹ &amp;ldquo;Báº¥t Ä‘á»™ng sáº£n tá»‘t nháº¥t tháº¿ giá»›i&amp;rdquo; lu&amp;ocirc;n l&amp;agrave; Ä‘&amp;iacute;ch pháº¥n Ä‘áº¥u cá»§a c&amp;aacute;c dá»± &amp;aacute;n báº¥t Ä‘á»™ng sáº£n tr&amp;ecirc;n tháº¿ giá»›i.&lt;/p&gt;\r\n\r\n&lt;p&gt;TrÆ°á»›c Ä‘&amp;oacute;, Vinhomes Riverside Ä‘&amp;atilde; láº§n lÆ°á»£t vÆ°á»£t qua c&amp;aacute;c Ä‘á»‘i thá»§ xuáº¥t sáº¯c nháº¥t ch&amp;acirc;u lá»¥c v&amp;agrave; tháº¿ giá»›i Ä‘á»ƒ Ä‘Æ°á»£c vinh danh á»Ÿ c&amp;aacute;c háº¡ng má»¥c &amp;ldquo;Khu Ä‘&amp;ocirc; thá»‹ tá»‘t nháº¥t Ch&amp;acirc;u &amp;Aacute; &amp;ndash; Th&amp;aacute;i B&amp;igrave;nh DÆ°Æ¡ng&amp;rdquo; (Asia Pacific&amp;rsquo;s Best Residential Property), v&amp;agrave; sau Ä‘&amp;oacute; l&amp;agrave; &amp;ldquo;Khu Ä‘&amp;ocirc; thá»‹ tá»‘t nháº¥t Quá»‘c táº¿&amp;rdquo; (Best International Residential Property).&lt;/p&gt;\r\n\r\n&lt;p&gt;Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o tháº¿ giá»›i b&amp;igrave;nh chá»n cho Vinhomes Riverside sau khi Ä‘&amp;aacute;nh gi&amp;aacute; ká»¹ lÆ°á»¡ng c&amp;aacute;c Ä‘iá»ƒm Æ°u viá»‡t cá»§a há»‡ sinh th&amp;aacute;i to&amp;agrave;n diá»‡n v&amp;agrave; hÆ¡n 100 tiá»‡n &amp;iacute;ch ná»™i khu Ä‘a dáº¡ng Ä‘Æ°á»£c quy hoáº¡ch Ä‘á»“ng bá»™, khoa há»c. Äáº·c biá»‡t, cáº£nh quan xanh m&amp;aacute;t vá»›i hÆ¡n 70ha c&amp;acirc;y xanh; 12,4ha máº·t há»“ Harmony v&amp;agrave; 18,8km k&amp;ecirc;nh Ä‘&amp;agrave;o táº¡i Vinhomes Riverside Ä‘&amp;atilde; thuyáº¿t phá»¥c ho&amp;agrave;n to&amp;agrave;n c&amp;aacute;c chuy&amp;ecirc;n gia báº¥t Ä‘á»™ng sáº£n h&amp;agrave;ng Ä‘áº§u tháº¿ giá»›i.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/2018/Vinhomes/IPA_Anh%202.JPG&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;em&gt;Vinhomes Riverside Ä‘&amp;atilde; thuyáº¿t phá»¥c ho&amp;agrave;n to&amp;agrave;n c&amp;aacute;c chuy&amp;ecirc;n gia báº¥t Ä‘á»™ng sáº£n h&amp;agrave;ng Ä‘áº§u tháº¿ giá»›i nhá» há»‡ sinh th&amp;aacute;i to&amp;agrave;n diá»‡n Ä‘Æ°á»£c quy hoáº¡ch Ä‘á»“ng bá»™, khoa há»c v&amp;agrave; há»‡ thá»‘ng cáº£nh quan xanh m&amp;aacute;t.&amp;nbsp;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Viá»‡c Vinhomes Riverside li&amp;ecirc;n tiáº¿p gi&amp;agrave;nh vá»‹ tr&amp;iacute; sá»‘ má»™t á»Ÿ c&amp;aacute;c háº¡ng má»¥c giáº£i thÆ°á»Ÿng danh gi&amp;aacute; á»Ÿ lÄ©nh vá»±c &amp;ldquo;Khu Ä‘&amp;ocirc; thá»‹&amp;rdquo; v&amp;agrave; Ä‘i Ä‘áº¿n ng&amp;ocirc;i vá»‹ cao nháº¥t cá»§a há»‡ thá»‘ng IPA l&amp;agrave; th&amp;agrave;nh tá»±u má»›i cá»§a báº¥t Ä‘á»™ng sáº£n Viá»‡t Nam. TrÆ°á»›c Ä‘&amp;oacute;, chÆ°a tá»«ng c&amp;oacute; dá»± &amp;aacute;n Viá»‡t Nam n&amp;agrave;o Ä‘áº¡t Ä‘Æ°á»£c giáº£i thÆ°á»Ÿng &amp;ldquo;Báº¥t Ä‘á»™ng sáº£n tá»‘t nháº¥t tháº¿ giá»›i&amp;rdquo; - (World&amp;rsquo;s Best Property).&lt;/p&gt;\r\n\r\n&lt;p&gt;B&amp;agrave; Nguyá»…n Diá»‡u Linh &amp;ndash; Tá»•ng gi&amp;aacute;m Ä‘á»‘c C&amp;ocirc;ng ty Vinhomes chia sáº»&lt;em&gt;: &amp;ldquo;Vinhomes Riverside&lt;/em&gt; &lt;em&gt;Ä‘áº¡t giáº£i thÆ°á»Ÿng&amp;ldquo;Báº¥t Ä‘á»™ng sáº£n tá»‘t nháº¥t tháº¿ giá»›i&amp;rdquo; nÄƒm 2018 kh&amp;ocirc;ng chá»‰ l&amp;agrave; th&amp;agrave;nh quáº£ v&amp;agrave; niá»m tá»± h&amp;agrave;o cá»§a Táº­p Ä‘o&amp;agrave;n Vingroup. &lt;/em&gt;&lt;em&gt;Giáº£i thÆ°á»Ÿng Ä‘&amp;atilde; kháº³ng Ä‘á»‹nh táº§m v&amp;oacute;c v&amp;agrave; nÄƒng lá»±c cá»§a ng&amp;agrave;nh báº¥t Ä‘á»™ng sáº£n Viá»‡t Nam, cho tháº¥y ch&amp;uacute;ng ta Ä‘&amp;atilde; s&amp;aacute;nh ngang, tháº­m ch&amp;iacute; vÆ°á»£t tr&amp;ecirc;n nhiá»u dá»± &amp;aacute;n báº¥t Ä‘á»™ng sáº£n quá»‘c táº¿ trong viá»‡c kiáº¿n táº¡o ra nhá»¯ng kh&amp;ocirc;ng gian Ä‘&amp;aacute;ng sá»‘ng nháº¥t&amp;rdquo;.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/2018/Vinhomes/IPA_Anh%203.JPG&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;em&gt;Há»‡ thá»‘ng cáº£nh quan xanh m&amp;aacute;t vá»›i hÆ¡n 70ha c&amp;acirc;y xanh; 12,4ha máº·t há»“ Harmony v&amp;agrave; 18,8km k&amp;ecirc;nh Ä‘&amp;agrave;o gi&amp;uacute;p Vinhomes Riverside vÆ°á»£t qua nhiá»u Ä‘á»‘i thá»§ máº¡nh Ä‘á»ƒ gi&amp;agrave;nh Ä‘Æ°á»£c giáº£i thÆ°á»Ÿng danh gi&amp;aacute; báº­c nháº¥t tháº¿ giá»›i.&lt;/em&gt;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;IPA l&amp;agrave; há»‡ thá»‘ng giáº£i thÆ°á»Ÿng báº¥t Ä‘á»™ng sáº£n danh gi&amp;aacute; báº­c nháº¥t tr&amp;ecirc;n tháº¿ giá»›i, Ä‘Æ°á»£c c&amp;ocirc;ng nháº­n tr&amp;ecirc;n to&amp;agrave;n cáº§u v&amp;agrave; l&amp;agrave; báº£o chá»©ng uy t&amp;iacute;n cá»§a c&amp;aacute;c doanh nghiá»‡p báº¥t Ä‘á»™ng sáº£n. Giáº£i thÆ°á»Ÿng vinh danh c&amp;aacute;c c&amp;ocirc;ng tr&amp;igrave;nh kiáº¿n tr&amp;uacute;c, thiáº¿t káº¿ ná»™i tháº¥t, chá»§ Ä‘áº§u tÆ°/tÆ° váº¥n/ marketing &amp;amp; website báº¥t Ä‘á»™ng sáº£n, khu Ä‘&amp;ocirc; thá»‹/ thÆ°Æ¡ng máº¡i/ nghá»‰ dÆ°á»¡ng/ s&amp;acirc;n golf; vá»›i ti&amp;ecirc;u ch&amp;iacute; cháº¥t lÆ°á»£ng dá»‹ch vá»¥, ph&amp;aacute;t triá»ƒn bá»n vá»¯ng, ti&amp;ecirc;u chuáº©n sá»‘ng cao, t&amp;iacute;nh Ä‘á»™c Ä‘&amp;aacute;o v&amp;agrave; s&amp;aacute;ng táº¡o. Ä&amp;acirc;y cÅ©ng ch&amp;iacute;nh l&amp;agrave; nhá»¯ng ti&amp;ecirc;u ch&amp;iacute; h&amp;agrave;ng Ä‘áº§u m&amp;agrave; Táº­p Ä‘o&amp;agrave;n Vingroup Ä‘á» cao khi quy hoáº¡ch ph&amp;aacute;t triá»ƒn khu Ä‘&amp;ocirc; thá»‹ Vinhomes Riveside cÅ©ng nhÆ° c&amp;aacute;c dá»± &amp;aacute;n báº¥t Ä‘á»™ng sáº£n kh&amp;aacute;c.&lt;/p&gt;\r\n\r\n&lt;p&gt;Tá»« nÄƒm 2012 Ä‘áº¿n nay, c&amp;aacute;c dá»± &amp;aacute;n cá»§a Vingroup li&amp;ecirc;n tiáº¿p Ä‘Æ°á»£c IPA vinh danh h&amp;agrave;ng Ä‘áº§u khu vá»±c v&amp;agrave; tháº¿ giá»›i á»Ÿ c&amp;aacute;c háº¡ng má»¥c kh&amp;aacute;c nhau, tá»« &amp;ldquo;Dá»± &amp;aacute;n phá»©c há»£p tá»‘t nháº¥t&amp;rdquo;, &amp;ldquo;Dá»± &amp;aacute;n c&amp;oacute; cáº£nh quan tá»‘t nháº¥t&amp;rdquo;, &amp;ldquo;T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t&amp;rdquo; Ä‘áº¿n &amp;ldquo;Chá»§ Ä‘áº§u tÆ° TTTM tá»‘t nháº¥t&amp;rdquo;...&lt;/p&gt;\r\n\r\n&lt;p&gt;Viá»‡c Vinhomes Riverside Ä‘Æ°á»£c IPA vinh danh á»Ÿ háº¡ng má»¥c giáº£i thÆ°á»Ÿng &amp;ldquo;Báº¥t Ä‘á»™ng sáº£n tá»‘t nháº¥t tháº¿ giá»›i&amp;rdquo; 2018 kh&amp;ocirc;ng chá»‰ t&amp;aacute;i kháº³ng Ä‘á»‹nh vá»‹ tr&amp;iacute; nh&amp;agrave; ph&amp;aacute;t triá»ƒn BÄS sá»‘ 1 Viá»‡t Nam cá»§a Vingroup m&amp;agrave; c&amp;ograve;n cho tháº¥y c&amp;aacute;c dá»± &amp;aacute;n cá»§a má»™t Chá»§ Ä‘áº§u tÆ° Báº¥t Ä‘á»™ng sáº£n Ä‘áº¿n tá»« Viá»‡t Nam ho&amp;agrave;n to&amp;agrave;n c&amp;oacute; thá»ƒ tá»± tin chinh phá»¥c nhá»¯ng Ä‘á»‰nh cao cá»§a báº¥t Ä‘á»™ng sáº£n tháº¿ giá»›i./.&lt;/p&gt;\r\n', 'Screenshot_3.png', '2019/06/21 00:27:48', 'Tá»‘i 03/12/2018, táº¡i Lá»… trao giáº£i thÆ°á»Ÿng Báº¥t Ä‘á»™ng sáº£n Quá»‘c táº¿ - International Property Awards tá»• chá»©c táº¡i London, Khu Ä‘&ocirc; thá»‹ sinh th&aacute;i Vinhomes Riverside Ä‘&atilde; Ä‘Æ°á»£c vinh danh l&agrave; &ldquo;Báº¥t Ä‘á»™', 'VINHOMES RIVERSIDE'),
(7, 'VINHOMES OCEAN PARK ÄÆ¯á»¢C VINH DANH â€œDá»° ÃN PHá»¨C Há»¢P Tá»T NHáº¤T VIá»†T NAMâ€ Táº I APPA', '&lt;p&gt;VÆ°á»£t qua nhiá»u v&amp;ograve;ng Ä‘&amp;aacute;nh gi&amp;aacute; nghi&amp;ecirc;m ngáº·t cá»§a Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o quá»‘c táº¿, dá»± &amp;aacute;n &amp;ldquo;th&amp;agrave;nh phá»‘ biá»ƒn há»“&amp;rdquo; Vinhomes Ocean Park do Táº­p Ä‘o&amp;agrave;n Vingroup Ä‘áº§u tÆ° Ä‘&amp;atilde; vinh dá»± Ä‘á»©ng Ä‘áº§u háº¡ng má»¥c &amp;ldquo;&lt;strong&gt;&lt;em&gt;Dá»± &amp;aacute;n Phá»©c há»£p tá»‘t nháº¥t Viá»‡t Nam nÄƒm 2019 (Best mixed &amp;ndash; use development 2019)&lt;/em&gt;&lt;/strong&gt;.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/2019/May/APPA%201.jpg&quot; style=&quot;height:auto; width:300px&quot; /&gt;&lt;br /&gt;\r\n&lt;em&gt;Äáº¡i diá»‡n Vinhomes nháº­n giáº£i thÆ°á»Ÿng &amp;ldquo;Dá»± &amp;aacute;n Phá»©c há»£p tá»‘t nháº¥t Viá»‡t Nam nÄƒm 2019&amp;rdquo; cho dá»± &amp;aacute;n Vinhomes Ocean Park&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vinhomes Ocean Park Ä‘&amp;atilde; chinh phá»¥c Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o quá»‘c táº¿ bá»Ÿi há»‡ thá»‘ng tiá»‡n &amp;iacute;ch Ä‘á»“ng bá»™ v&amp;agrave; Ä‘áº§y Ä‘á»§, bao gá»“m: trÆ°á»ng há»c, bá»‡nh viá»‡n, TTTM, Shop/Shophouse,.. C&amp;ugrave;ng vá»›i Ä‘&amp;oacute; l&amp;agrave; h&amp;agrave;ng loáº¡t tiá»‡n &amp;iacute;ch Ä‘iá»ƒm nháº¥n láº§n Ä‘áº§u ti&amp;ecirc;n xuáº¥t hiá»‡n táº¡i Viá»‡t Nam nhÆ° c&amp;ocirc;ng vi&amp;ecirc;n gym vá»›i h&amp;agrave;ng trÄƒm m&amp;aacute;y táº­p, c&amp;ocirc;ng vi&amp;ecirc;n BBQ vá»›i h&amp;agrave;ng trÄƒm Ä‘iá»ƒm nÆ°á»›ng, h&amp;agrave;ng chá»¥c c&amp;ocirc;ng vi&amp;ecirc;n dÆ°á»¡ng sinh, s&amp;acirc;n chÆ¡i tráº» em,... Äáº·c biá»‡t, hai t&amp;acirc;m Ä‘iá»ƒm cá»§a dá»± &amp;aacute;n l&amp;agrave; biá»ƒn há»“ nÆ°á»›c máº·n 6,1ha v&amp;agrave; há»“ lá»›n trung t&amp;acirc;m 24,5ha Ä‘á»u Ä‘Æ°á»£c tráº£i c&amp;aacute;t tráº¯ng nhÆ° b&amp;atilde;i biá»ƒn Ä‘&amp;atilde; thuyáº¿t phá»¥c ho&amp;agrave;n to&amp;agrave;n Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o báº±ng t&amp;iacute;nh t&amp;aacute;o báº¡o v&amp;agrave; Ä‘á»™t ph&amp;aacute; trong &amp;yacute; tÆ°á»Ÿng thiáº¿t káº¿.&lt;/p&gt;\r\n\r\n&lt;p&gt;Äáº¡i diá»‡n Chá»§ Ä‘áº§u tÆ°, b&amp;agrave; Nguyá»…n Diá»‡u Linh - Ph&amp;oacute; Chá»§ tá»‹ch Táº­p Ä‘o&amp;agrave;n Vingroup, Chá»§ tá»‹ch c&amp;ocirc;ng ty Cá»• pháº§n Vinhomes chia sáº»: &amp;ldquo;Ch&amp;uacute;ng t&amp;ocirc;i thá»±c sá»± vui má»«ng khi má»™t láº§n ná»¯a, th&amp;ecirc;m má»™t dá»± &amp;aacute;n báº¥t Ä‘á»™ng sáº£n Vinhomes láº¡i d&amp;agrave;nh háº¡ng xá»©ng Ä‘&amp;aacute;ng táº¡i APPA 2019. Kh&amp;ocirc;ng dá»«ng láº¡i á»Ÿ má»™t thÆ°Æ¡ng hiá»‡u báº¥t sáº£n dáº«n dáº¯t thá»‹ trÆ°á»ng trong nÆ°á»›c, Vinhomes trong nhá»¯ng nÄƒm gáº§n Ä‘&amp;acirc;y li&amp;ecirc;n tá»¥c táº¡o Ä‘Æ°á»£c tiáº¿ng vang tr&amp;ecirc;n trÆ°á»ng quá»‘c táº¿ vá»›i nhiá»u dá»± &amp;aacute;n chiáº¿n tháº¯ng táº¡i c&amp;aacute;c háº¡ng má»¥c giáº£i thÆ°á»Ÿng danh gi&amp;aacute; to&amp;agrave;n cáº§u.&amp;rdquo;&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ugrave;ng Vinhomes Ocean Park, Vinhomes Star City, Vinhomes Smart City v&amp;agrave; Vinhomes Grand Park láº§n lÆ°á»£t Ä‘Æ°á»£c vinh danh táº¡i c&amp;aacute;c háº¡ng má»¥c giáº£i thÆ°á»Ÿng Dá»± &amp;aacute;n c&amp;oacute; kiáº¿n tr&amp;uacute;c phá»©c há»£p tá»‘t cá»§a Viá»‡t Nam, Dá»± &amp;aacute;n cao táº§ng tá»‘t cá»§a Viá»‡t Nam v&amp;agrave; Dá»± &amp;aacute;n cao táº§ng c&amp;oacute; thiáº¿t káº¿ cáº£nh quan tá»‘t cá»§a Viá»‡t Nam.&lt;/p&gt;\r\n\r\n&lt;p&gt;Giáº£i thÆ°á»Ÿng báº¥t Ä‘á»™ng sáº£n Ch&amp;acirc;u &amp;Aacute; - Th&amp;aacute;i B&amp;igrave;nh DÆ°Æ¡ng APPA thuá»™c há»‡ thá»‘ng giáº£i thÆ°á»Ÿng báº¥t Ä‘á»™ng sáº£n danh gi&amp;aacute; v&amp;agrave; uy t&amp;iacute;n báº­c nháº¥t tr&amp;ecirc;n tháº¿ giá»›i International Property Awards. Trong nhiá»u ti&amp;ecirc;u ch&amp;iacute; Ä‘&amp;aacute;nh gi&amp;aacute;, giáº£i thÆ°á»Ÿng Ä‘á» cao c&amp;aacute;c ti&amp;ecirc;u ch&amp;iacute; vá» cháº¥t lÆ°á»£ng cuá»™c sá»‘ng cho cÆ° d&amp;acirc;n. APPA Ä‘Æ°á»£c tá»• chá»©c thÆ°á»ng ni&amp;ecirc;n Ä‘á»ƒ vinh danh c&amp;aacute;c chá»§ Ä‘áº§u tÆ° v&amp;agrave; dá»± &amp;aacute;n xuáº¥t sáº¯c trong lÄ©nh vá»±c ph&amp;aacute;t triá»ƒn báº¥t Ä‘á»™ng sáº£n.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/2019/May/APPA%202.jpg&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;em&gt;B&amp;atilde;i c&amp;aacute;t tráº¯ng má»‹n tráº£i d&amp;agrave;i táº¡i há»“ Ä‘iá»u h&amp;ograve;a rá»™ng 24.5ha &amp;ndash; má»™t Ä‘iá»ƒm nháº¥n Ä‘á»™c Ä‘&amp;aacute;o gi&amp;uacute;p Vinhomes Ocean Park ghi Ä‘iá»ƒm táº¡i APPA 2019.&amp;nbsp;&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;TrÆ°á»›c Vinhomes Ocean Park, nhiá»u dá»± &amp;aacute;n cá»§a Vinhomes Ä‘&amp;atilde; Ä‘Æ°á»£c vinh danh trong há»‡ thá»‘ng giáº£i thÆ°á»Ÿng BÄS danh gi&amp;aacute; IPA nhÆ° Vinhomes Metropolis vá»›i giáº£i thÆ°á»Ÿng T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t Ch&amp;acirc;u &amp;Aacute; &amp;ndash; Th&amp;aacute;i B&amp;igrave;nh DÆ°Æ¡ng nÄƒm 2017, Landmark 81 vá»›i giáº£i thÆ°á»ng T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t tháº¿ giá»›i nÄƒm 2017, Vinhomes Riverside vá»›i giáº£i thÆ°á»Ÿng Khu Ä‘&amp;ocirc; thá»‹ tá»‘t nháº¥t tháº¿ giá»›i nÄƒm 2018.&lt;/p&gt;\r\n\r\n&lt;p&gt;Ch&amp;iacute;nh thá»©c khá»Ÿi c&amp;ocirc;ng tá»« th&amp;aacute;ng 10/2018, cho tá»›i nay chá»‰ sau 8 th&amp;aacute;ng thi c&amp;ocirc;ng, Vinhomes Ocean Park Ä‘&amp;atilde; th&amp;agrave;nh h&amp;igrave;nh vá»›i c&amp;aacute;c cung Ä‘Æ°á»ng ná»™i khu rá»™ng lá»›n rá»™ng 30m, 40m, 50m tráº£i nhá»±a v&amp;agrave; ho&amp;agrave;n thiá»‡n máº·t Ä‘Æ°á»ng chá»‰n chu. C&amp;aacute;c t&amp;ograve;a cÄƒn há»™ Ä‘&amp;atilde; l&amp;ecirc;n Ä‘áº¿n táº§ng 13,14 vá»›i c&amp;aacute;c khá»‘i nh&amp;agrave; Ä‘Æ°á»£c h&amp;igrave;nh th&amp;agrave;nh vá»¯ng ch&amp;atilde;i. C&amp;aacute;c c&amp;ocirc;ng tr&amp;igrave;nh trá»ng Ä‘iá»ƒm trong dá»± &amp;aacute;n nhÆ° Äáº¡i há»c VinUni cÅ©ng Ä‘ang Ä‘Æ°á»£c triá»ƒn khai thi c&amp;ocirc;ng nhanh ch&amp;oacute;ng.&lt;/p&gt;\r\n\r\n&lt;p&gt;Äáº·c biá»‡t, chá»‰ sau gáº§n 70 ng&amp;agrave;y thi c&amp;ocirc;ng tháº§n tá»‘c, cáº£nh quan trung t&amp;acirc;m bao gá»“m há»“ Ä‘iá»u h&amp;ograve;a rá»™ng 24.5ha Ä‘&amp;atilde; gáº§n nhÆ° ho&amp;agrave;n táº¥t. Bao quanh máº·t há»“ xanh m&amp;aacute;t l&amp;agrave; bá» c&amp;aacute;t tráº¯ng má»‹n tráº£i d&amp;agrave;i Ä‘Æ°á»£c ká»³ c&amp;ocirc;ng váº­n chuyá»ƒn tá»« biá»ƒn Nha Trang, tráº£i qua nhiá»u c&amp;ocirc;ng Ä‘oáº¡n s&amp;agrave;ng sáº£y ká»¹ c&amp;agrave;ng nhiá»u lá»›p Ä‘á»ƒ Ä‘áº¡t Ä‘Æ°á»£c m&amp;agrave;u c&amp;aacute;t tráº¯ng tá»± nhi&amp;ecirc;n nhÆ° á»Ÿ biá»ƒn tháº­t. H&amp;agrave;ng trÄƒm gá»‘c dá»«a Ä‘Æ°á»£c bá»‘ tr&amp;iacute; trá»“ng Ä‘an xen quanh há»“ Ä‘á»ƒ táº¡o n&amp;ecirc;n cáº£nh quan thi&amp;ecirc;n nhi&amp;ecirc;n nhÆ° b&amp;atilde;i biá»ƒn giá»¯a l&amp;ograve;ng th&amp;agrave;nh phá»‘. Hiá»‡n táº¡i, háº§u háº¿t c&amp;aacute;c háº¡ng má»¥c cáº£nh quan Ä‘iá»ƒm nháº¥n quanh há»“ Ä‘á»u Ä‘&amp;atilde; ho&amp;agrave;n thiá»‡n, sáºµn s&amp;agrave;ng cho lá»… khai trÆ°Æ¡ng v&amp;agrave;o ng&amp;agrave;y 25/05/2019 vá»›i má»™t Ä‘&amp;ecirc;m Ä‘áº¡i nháº¡c há»™i ho&amp;agrave;nh tr&amp;aacute;ng b&amp;ecirc;n há»“ d&amp;agrave;nh cho hÆ¡n 3000 cÆ° d&amp;acirc;n Th&amp;agrave;nh phá»‘ Biá»ƒn há»“ Vinhomes Ocean Park.&lt;/p&gt;\r\n', 'tin.jpg', '2019/06/21 00:22:22', 'Má»›i Ä‘&acirc;y, táº¡i lá»… trao giáº£i thÆ°á»Ÿng BÄS Ch&acirc;u &Aacute; - Th&aacute;i B&igrave;nh DÆ°Æ¡ng (APPA) diá»…n ra táº¡i Bangkok (Th&aacute;i Lan), dá»± &aacute;n &ldquo;th&agrave;nh phá»‘ biá»ƒn há»“&rdquo; Vinhomes Ocean Park Ä‘&atilde; Ä‘Æ°', 'VINHOMES OCEAN PARK'),
(8, 'VINHOMES METROPOLIS - â€œTÃ’A NHÃ€ CAO Táº¦NG Tá»T NHáº¤T CHÃ‚U Ã THÃI BÃŒNH DÆ¯Æ NGâ€ 2017', '&lt;p&gt;VÆ°á»£t qua c&amp;aacute;c v&amp;ograve;ng Ä‘&amp;aacute;nh gi&amp;aacute; nghi&amp;ecirc;m ngáº·t cá»§a Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o quá»‘c táº¿ International Property Awards (IPA) v&amp;agrave; nhá»¯ng Ä‘á» cá»­ ná»•i báº­t cáº¥p quá»‘c gia v&amp;agrave; khu vá»±c - Vinhomes Metropolis Ä‘&amp;atilde; gi&amp;agrave;nh chiáº¿n tháº¯ng thuyáº¿t phá»¥c táº¡i háº¡ng má»¥c &lt;strong&gt;&amp;ldquo;&lt;/strong&gt;T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t Ch&amp;acirc;u &amp;Aacute; Th&amp;aacute;i B&amp;igrave;nh DÆ°Æ¡ng&amp;quot; 2017.&lt;br /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/Batdongsan/Metropolis/IMG_1279.jpg&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;em&gt;Ban tá»• chá»©c trao chá»©ng nháº­n giáº£i thÆ°á»Ÿng cho Ä‘áº¡i diá»‡n Chá»§ Ä‘áº§u tÆ°.&lt;/em&gt;&lt;br /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ocirc;ng tr&amp;igrave;nh tá»a láº¡c táº¡i 29 Liá»…u Giai, H&amp;agrave; Ná»™i vá»›i táº§m nh&amp;igrave;n Ä‘á»™c Ä‘&amp;aacute;o 360 Ä‘á»™ hÆ°á»›ng ra 4 há»“ lá»›n cá»§a Thá»§ Ä‘&amp;ocirc;. C&amp;ugrave;ng vá»›i vá»‹ tr&amp;iacute; Ä‘áº¯c Ä‘á»‹a, Vinhomes Metropolis Ä‘&amp;atilde; chinh phá»¥c Ä‘Æ°á»£c Há»™i Ä‘á»“ng gi&amp;aacute;m kháº£o bá»Ÿi thiáº¿t káº¿ th&amp;ocirc;ng minh, sang trá»ng v&amp;agrave; tinh táº¿. Dá»± &amp;aacute;n Ä‘Æ°á»£c quy hoáº¡ch th&amp;agrave;nh má»™t tá»• há»£p VÄƒn ph&amp;ograve;ng &amp;ndash; TTTM &amp;ndash; cÄƒn há»™ cao cáº¥p, gá»“m gá»“m 3 t&amp;ograve;a th&amp;aacute;p cÄƒn há»™ M1, M2, M3. C&amp;aacute;c khá»‘i nh&amp;agrave; Ä‘Æ°á»£c li&amp;ecirc;n káº¿t báº±ng nhá»¯ng khoáº£ng kh&amp;ocirc;ng gian xanh v&amp;agrave; há»‡ thá»‘ng tiá»‡n &amp;iacute;ch ngo&amp;agrave;i trá»i, táº¡o n&amp;ecirc;n má»™t quáº§n thá»ƒ phá»©c há»£p Ä‘áº³ng cáº¥p. B&amp;ecirc;n ngo&amp;agrave;i c&amp;aacute;c t&amp;ograve;a th&amp;aacute;p trang bá»‹ há»‡ k&amp;iacute;nh há»™p Low-E vá»›i t&amp;aacute;c dá»¥ng cáº£n nhiá»‡t, cáº£n tia cá»±c t&amp;iacute;m, b&amp;ecirc;n trong cÄƒn há»™ c&amp;oacute; ná»™i tháº¥t liá»n tÆ°á»ng sang trá»ng vá»›i há»‡ thá»‘ng Ä‘iá»u h&amp;ograve;a trung t&amp;acirc;m, thiáº¿t bá»‹ vá»‡ sinh cao cáº¥p thÆ°Æ¡ng hiá»‡u Duravit v&amp;agrave; Hansgrohe.&lt;/p&gt;\r\n\r\n&lt;p&gt;B&amp;ecirc;n cáº¡nh yáº¿u tá»‘ cháº¥t lÆ°á»£ng v&amp;agrave; ká»¹ thuáº­t x&amp;acirc;y dá»±ng - kh&amp;ocirc;ng gian xanh táº¡i Vinhomes Metropolis Ä‘Æ°á»£c IPA Ä‘&amp;aacute;nh gi&amp;aacute; cao, g&amp;oacute;p pháº§n ghi Ä‘iá»ƒm cho giáº£i thÆ°á»Ÿng &amp;ldquo;T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t Ch&amp;acirc;u &amp;Aacute; Th&amp;aacute;i B&amp;igrave;nh DÆ°Æ¡ng 2017&amp;rdquo;. Dá»± &amp;aacute;n sá»Ÿ há»¯u há»‡ thá»‘ng cáº£nh quan ho&amp;agrave;n háº£o, vá»›i Ä‘iá»ƒm nháº¥n l&amp;agrave; khu vÆ°á»n ná»•i táº¡i táº§ng 4, li&amp;ecirc;n th&amp;ocirc;ng cáº£ 3 t&amp;ograve;a th&amp;aacute;p, vá»«a táº¡o ra cáº£nh quan xanh m&amp;aacute;t, vá»«a trá»Ÿ th&amp;agrave;nh biá»ƒu tÆ°á»£ng kiáº¿n tr&amp;uacute;c Ä‘áº·c trÆ°ng.&lt;br /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/Batdongsan/Metropolis/VHM2.jpg&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\n&lt;em&gt;Phá»‘i cáº£nh Vinhomes Metropolis&lt;/em&gt;&lt;br /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;CÅ©ng táº¡i sá»± kiá»‡n, t&amp;ograve;a th&amp;aacute;p cao 81 táº§ng Landmark 81 thuá»™c Khu Ä‘&amp;ocirc; thá»‹ Vinhomes Central Park má»™t láº§n ná»¯a Ä‘Æ°á»£c vinh danh l&amp;agrave; &amp;ldquo;C&amp;ocirc;ng tr&amp;igrave;nh kiáº¿n tr&amp;uacute;c cao táº§ng tá»‘t nháº¥t Tháº¿ giá»›i&amp;rdquo; trong pháº§n giáº£i thÆ°á»Ÿng d&amp;agrave;nh cho c&amp;ocirc;ng ty tÆ° váº¥n thiáº¿t káº¿ Atkins. NÄƒm 2016, cÅ©ng vá»›i T&amp;ograve;a th&amp;aacute;p Landmark 81, chá»§ Ä‘áº§u tÆ° Vingroup Ä‘&amp;atilde; gi&amp;agrave;nh giáº£i thÆ°á»Ÿng &amp;ldquo;T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t tháº¿ giá»›i&amp;quot;.&lt;/p&gt;\r\n\r\n&lt;p&gt;Ph&amp;aacute;t biá»ƒu vá» giáº£i thÆ°á»Ÿng, b&amp;agrave; DÆ°Æ¡ng Thá»‹ Mai Hoa - Tá»•ng Gi&amp;aacute;m Ä‘á»‘c Táº­p Ä‘o&amp;agrave;n Vingroup cho biáº¿t: &lt;em&gt;&amp;ldquo;Vá»›i 2 nÄƒm li&amp;ecirc;n tiáº¿p Ä‘Æ°á»£c vinh danh á»Ÿ háº¡ng má»¥c &amp;ldquo;T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t&amp;rdquo; khu &amp;nbsp;vá»±c v&amp;agrave; tháº¿ giá»›i, Vingroup tá»± h&amp;agrave;o g&amp;oacute;p pháº§n mang Ä‘áº¿n cháº¥t lÆ°á»£ng v&amp;agrave; chuáº©n má»±c sá»‘ng s&amp;aacute;nh ngang vá»›i c&amp;aacute;c nÆ°á»›c ph&amp;aacute;t triá»ƒn ngay táº¡i Viá»‡t Nam&amp;rdquo;.&lt;/em&gt;&lt;br /&gt;\r\n&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;&lt;img src=&quot;http://vingroup.net/Uploads/0_Tintuchoatdong/Batdongsan/Metropolis/Phoi%20canh%20khong%20gian%20xanh.jpeg&quot; style=&quot;height:auto; width:500px&quot; /&gt;&lt;br /&gt;\r\n&lt;br /&gt;\r\nPhá»‘i cáº£nh Vinhomes Metropolis&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;IPA l&amp;agrave; há»‡ thá»‘ng giáº£i thÆ°á»Ÿng báº¥t Ä‘á»™ng sáº£n danh gi&amp;aacute; báº­c nháº¥t tr&amp;ecirc;n tháº¿ giá»›i, Ä‘Æ°á»£c c&amp;ocirc;ng nháº­n tr&amp;ecirc;n to&amp;agrave;n cáº§u v&amp;agrave; l&amp;agrave; báº£o chá»©ng uy t&amp;iacute;n cá»§a c&amp;aacute;c doanh nghiá»‡p báº¥t Ä‘á»™ng sáº£n. Giáº£i thÆ°á»Ÿng vinh danh c&amp;aacute;c c&amp;ocirc;ng tr&amp;igrave;nh kiáº¿n tr&amp;uacute;c, thiáº¿t káº¿ ná»™i tháº¥t, chá»§ Ä‘áº§u tÆ°/ tÆ° váº¥n/ marketing &amp;amp; website báº¥t Ä‘á»™ng sáº£n, khu Ä‘&amp;ocirc; thá»‹/ thÆ°Æ¡ng máº¡i/ nghá»‰ dÆ°á»¡ng/ s&amp;acirc;n golf; vá»›i ti&amp;ecirc;u ch&amp;iacute; cháº¥t lÆ°á»£ng dá»‹ch vá»¥, ph&amp;aacute;t triá»ƒn bá»n vá»¯ng, ti&amp;ecirc;u chuáº©n sá»‘ng cao, t&amp;iacute;nh Ä‘á»™c Ä‘&amp;aacute;o v&amp;agrave; s&amp;aacute;ng táº¡o.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;br /&gt;\r\nTá»« nÄƒm 2012 Ä‘áº¿n nay, c&amp;aacute;c dá»± &amp;aacute;n cá»§a Vingroup li&amp;ecirc;n tiáº¿p Ä‘Æ°á»£c IPA vinh danh h&amp;agrave;ng Ä‘áº§u khu vá»±c v&amp;agrave; tháº¿ giá»›i á»Ÿ c&amp;aacute;c háº¡ng má»¥c kh&amp;aacute;c nhau, tá»« &amp;ldquo;Dá»± &amp;aacute;n phá»©c há»£p tá»‘t nháº¥t&amp;rdquo;, &amp;ldquo;Dá»± &amp;aacute;n c&amp;oacute; cáº£nh quan tá»‘t nháº¥t&amp;rdquo;, &amp;ldquo;T&amp;ograve;a nh&amp;agrave; cao táº§ng tá»‘t nháº¥t&amp;rdquo; Ä‘áº¿n &amp;ldquo;Chá»§ Ä‘áº§u tÆ° TTTM tá»‘t nháº¥t&amp;rdquo;... g&amp;oacute;p pháº§n kháº³ng Ä‘á»‹nh Ä‘áº³ng cáº¥p v&amp;agrave; vá»‹ tháº¿ cá»§a trong nhá»¯ng nh&amp;agrave; ph&amp;aacute;t triá»ƒn báº¥t Ä‘á»™ng sáº£n h&amp;agrave;ng Ä‘áº§u Viá»‡t Nam cá»§a Vingroup. /.&lt;/p&gt;\r\n', 'Screenshot_4.png', '2019/06/21 00:29:41', 'Dá»± &aacute;n Vinhomes Metropolis do Táº­p Ä‘o&agrave;n Vingroup Ä‘áº§u tÆ° vá»«a Ä‘Æ°á»£c vinh danh l&agrave; &ldquo;T&ograve;a nh&agrave; cao táº§ng tá»‘t nháº¥t Ch&acirc;u &Aacute; Th&aacute;i B&igrave;nh DÆ°Æ¡ng&rdquo; táº¡i Lá»… trao giáº£i thÆ°á»Ÿn', 'VINHOMES METROPOLIS');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_chitiethoadon`
--

CREATE TABLE `tbl_chitiethoadon` (
  `id` int(11) NOT NULL,
  `tai_khoan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `masp` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `soluong` int(11) NOT NULL,
  `dongia` int(11) NOT NULL,
  `diachi_giaodich` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_diachiduan`
--

CREATE TABLE `tbl_diachiduan` (
  `id` int(11) NOT NULL,
  `diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sodienthoai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_diachiduan`
--

INSERT INTO `tbl_diachiduan` (`id`, `diachi`, `sodienthoai`, `email`) VALUES
(2, 'Sá»‘ 12, ÄÆ°á»ng ChÃ¹a Bá»™c, Quáº­n Äá»‘ng Äa, HÃ  Ná»™i, Viá»‡t Nam', '18001800', 'bdshathanh.vn');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_diachigiaodich`
--

CREATE TABLE `tbl_diachigiaodich` (
  `id` int(11) NOT NULL,
  `diachi` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sodienthoai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_diachigiaodich`
--

INSERT INTO `tbl_diachigiaodich` (`id`, `diachi`, `sodienthoai`, `email`) VALUES
(1, 'Sá»‘ 12, ÄÆ°á»ng ChÃ¹a Bá»™c, Quáº­n Äá»‘ng Äa, HÃ  Ná»™i, Viá»‡t Nam', '0913500391', 'yennt@bdshathanh.vn');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_gioithieu`
--

CREATE TABLE `tbl_gioithieu` (
  `id` int(11) NOT NULL,
  `ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slogan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `anh` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `noidung` text COLLATE utf8_unicode_ci NOT NULL,
  `gioithieu2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ngaythang` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_gioithieu`
--

INSERT INTO `tbl_gioithieu` (`id`, `ten`, `slogan`, `anh`, `noidung`, `gioithieu2`, `ngaythang`) VALUES
(1, 'BÄS HÃ€ THÃ€NH', 'Táº¡o Dá»±ng Tá»• áº¤m Viá»‡t', 'teaser-slide.png', '&lt;h2&gt;Há»— trá»£ Ä‘áº¯c lá»±c cho báº¡n d&amp;ugrave; báº¡n á»Ÿ Ä‘&amp;acirc;u?&lt;/h2&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;C&amp;ocirc;ng nghá»‡ Ä‘ang trá»Ÿ th&amp;agrave;nh 1 pháº§n táº¥t yáº¿u v&amp;agrave; gi&amp;uacute;p cuá»™c sá»‘ng cá»§a ch&amp;uacute;ng ta tá»‘t Ä‘áº¹p hÆ¡n. C&amp;ocirc;ng nghá»‡ cÅ©ng mang Ä‘áº¿n cho lÄ©nh vá»±c báº¥t Ä‘á»™ng sáº£n má»™t phÆ°Æ¡ng thá»©c giao dá»‹ch má»›i, nhanh gá»n, tiá»‡n &amp;iacute;ch, tiáº¿t kiá»‡m chi ph&amp;iacute; v&amp;agrave; minh báº¡ch táº¡i bdshathanh.vn.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Tá»« th&amp;agrave;nh quáº£ cá»§a cuá»™c c&amp;aacute;ch máº¡ng c&amp;ocirc;ng nghiá»‡p 4.0, BÄS H&amp;agrave; Th&amp;agrave;nh x&amp;acirc;y dá»±ng má»™t ná»n táº£ng kinh doanh báº¥t Ä‘á»™ng sáº£n hiá»‡n Ä‘áº¡i báº±ng sá»± káº¿t há»£p giá»¯a c&amp;ocirc;ng nghá»‡ v&amp;agrave; Ä‘á»‹nh hÆ°á»›ng ph&amp;aacute;t triá»ƒn cá»§a nhá»¯ng chuy&amp;ecirc;n gia h&amp;agrave;ng Ä‘áº§u. Vá»›i viá»‡c cung cáº¥p t&amp;agrave;i liá»‡u, trang thiáº¿t bá»‹ v&amp;agrave; há»— trá»£ truyá»n th&amp;ocirc;ng, BÄS H&amp;agrave; Th&amp;agrave;nh táº¡o Ä‘iá»u kiá»‡n tá»‘t nháº¥t Ä‘á»ƒ c&amp;aacute;c Äáº¡i l&amp;yacute; á»§y quyá»n c&amp;oacute; tá»‘i Ä‘a thá»i gian Ä‘á»ƒ tÆ° váº¥n v&amp;agrave; há»— trá»£ kh&amp;aacute;ch h&amp;agrave;ng.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Vá»›i BÄS H&amp;agrave; Th&amp;agrave;nh, kh&amp;aacute;ch b&amp;aacute;n c&amp;oacute; thá»ƒ dá»… d&amp;agrave;ng Ä‘á»‹nh gi&amp;aacute; sáº£n pháº©m ch&amp;iacute;nh x&amp;aacute;c báº±ng c&amp;ocirc;ng nghá»‡ v&amp;agrave; gá»­i b&amp;aacute;n Ä‘&amp;uacute;ng gi&amp;aacute;. Kh&amp;aacute;ch mua dá»… d&amp;agrave;ng t&amp;igrave;m kiáº¿m c&amp;aacute;c sáº£n pháº©m ph&amp;ugrave; há»£p, Ä‘&amp;uacute;ng nhu cáº§u. Äá»™i ngÅ© há»— trá»£, tÆ° váº¥n l&amp;agrave; c&amp;aacute;c chuy&amp;ecirc;n gia tÆ° váº¥n (Mentor) v&amp;agrave; Äáº¡i l&amp;yacute; á»§y quyá»n sáº½ gi&amp;uacute;p c&amp;aacute;c giao dá»‹ch Ä‘Æ°á»£c thá»±c hiá»‡n nhanh ch&amp;oacute;ng, dá»… d&amp;agrave;ng, tiáº¿t kiá»‡m thá»i gian v&amp;agrave; c&amp;ocirc;ng sá»©c.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;Sá»© má»‡nh cá»§a BÄS H&amp;agrave; Th&amp;agrave;nh l&amp;agrave; ti&amp;ecirc;n phong kiáº¿n táº¡o nhá»¯ng c&amp;ocirc;ng tr&amp;igrave;nh vá»›i Ä‘áº³ng cáº¥p v&amp;agrave; cháº¥t lÆ°á»£ng vÆ°á»£t trá»™i, Ä‘á»“ng thá»i x&amp;acirc;y dá»±ng n&amp;ecirc;n má»™t thÆ°Æ¡ng hiá»‡u Viá»‡t bá»n vá»¯ng, c&amp;oacute; uy t&amp;iacute;n, Ä‘áº³ng cáº¥p trong khu vá»±c v&amp;agrave; quá»‘c táº¿.&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;a href=&quot;http://www.mik.vn/gioi-thieu.html&quot;&gt;T&amp;Igrave;M HIá»‚U TH&amp;Ecirc;M&lt;/a&gt;&lt;/p&gt;\r\n', '', '2019/06/21 02:02:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_hoadon`
--

CREATE TABLE `tbl_hoadon` (
  `id` int(11) NOT NULL,
  `ngayhoadon` date NOT NULL,
  `tongtien` float NOT NULL,
  `tai_khoan` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_hoadon`
--

INSERT INTO `tbl_hoadon` (`id`, `ngayhoadon`, `tongtien`, `tai_khoan`) VALUES
(1, '0000-00-00', 432423, 'fgsfdgsf'),
(2, '2019-06-11', 2222, 'admin');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_khachhang`
--

CREATE TABLE `tbl_khachhang` (
  `ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sodienthoai` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `noidungphanhoi` text COLLATE utf8_unicode_ci NOT NULL,
  `ngaythang` date NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_khachhang`
--

INSERT INTO `tbl_khachhang` (`ten`, `sodienthoai`, `email`, `noidungphanhoi`, `ngaythang`, `id`) VALUES
('sdfasd', '534', 'phucnguyenbk98@gmail.com', 'sadasdsadasd', '2019-06-19', 1),
('$fullname', '$dienthoai', '$email', '$noidung', '0000-00-00', 3),
('sdfasd', '534', 'phucnguyenbk98@gmail.com', 'sadasdsadasd', '2019-06-19', 4),
('sdfasd', '534', 'phucnguyenbk98@gmail.com', 'sadasdsadasd', '2019-06-19', 5),
('rfsdaf', '43', 'phucnguyenbk98@gmail.com', 'fcsd', '2019-06-19', 6),
('rfsdaf', '43', 'phucnguyenbk98@gmail.com', 'fcsd', '2019-06-19', 7),
('rfsdaf', '43', 'phucnguyenbk98@gmail.com', 'fcsd', '2019-06-19', 8),
('rfsdaf', '43', 'phucnguyenbk98@gmail.com', 'fcsd', '2019-06-19', 9),
('sdfasd', '534', 'phucnguyenbk98@gmail.com', 'sadasdsadasd', '2019-06-19', 10),
('sdfasd', '534', 'phucnguyenbk98@gmail.com', 'sadasdsadasd', '2019-06-19', 11),
('rfsdaf', '1111', 'phucnguyenbk98@gmail.com', 'tttt', '2019-06-19', 12),
('rfsdaf', '1111', 'phucnguyenbk98@gmail.com', 'tttt', '2019-06-19', 13),
('rfsdaf', '1111', 'phucnguyenbk98@gmail.com', 'tttt', '2019-06-19', 14),
('rfsdaf', '1111', 'phucnguyenbk98@gmail.com', 'tttt', '2019-06-19', 15),
('rfsdaf', '3', 'hoaithuong@GMSAIL', 'Æ°ef', '2019-06-20', 16),
('rfsdaf', '3', 'hoaithuong@GMSAIL', 'Æ°ef', '2019-06-20', 17);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_nguoidung`
--

CREATE TABLE `tbl_nguoidung` (
  `id` int(11) NOT NULL,
  `tai_khoan` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mat_khau` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ho_ten` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ngay_sinh` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gioi_tinh` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_nguoidung`
--

INSERT INTO `tbl_nguoidung` (`id`, `tai_khoan`, `mat_khau`, `email`, `ho_ten`, `ngay_sinh`, `gioi_tinh`) VALUES
(4, 'admin', '123', 'phucnguyenbk98@gmail.com', 'dpn', '1998-03-08', 'nam'),
(5, 'phucnguyen', '12345', 'phucnguyenbkkkk98@gmail.com', '', '', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tbl_sanpham`
--

CREATE TABLE `tbl_sanpham` (
  `id` int(11) NOT NULL,
  `tensp` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `masp` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `chitietsanpham` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `gia` float NOT NULL,
  `soluong` int(11) NOT NULL,
  `anh1` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `anh2` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `anh3` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `ngaythang` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `chuyenmuc` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf32 COLLATE=utf32_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tbl_sanpham`
--

INSERT INTO `tbl_sanpham` (`id`, `tensp`, `masp`, `chitietsanpham`, `gia`, `soluong`, `anh1`, `anh2`, `anh3`, `ngaythang`, `chuyenmuc`) VALUES
(6, 'vinhomes-greenvillas', '12349977334', '&lt;p&gt;&lt;a href=&quot;http://bdscaocap.vn/&quot;&gt;&lt;strong&gt;Vinhomes Green Villas&lt;/strong&gt;&lt;/a&gt;&amp;nbsp;&amp;ndash; l&amp;agrave; quáº§n thá»ƒ&amp;nbsp;&lt;strong&gt;biá»‡t thá»± Ä‘Æ¡n láº­p&lt;/strong&gt;&amp;nbsp;kh&amp;eacute;p k&amp;iacute;n, sang trá»ng Ä‘em láº¡i cuá»™c sá»‘ng Ä‘á»‰nh cao ri&amp;ecirc;ng tÆ° cho nhá»¯ng kh&amp;aacute;ch h&amp;agrave;ng tinh hoa trong x&amp;atilde; há»™i.thi&amp;ecirc;n Ä‘Æ°á»ng sá»‘ng Ä‘&amp;iacute;ch thá»±c, nÆ¡i táº¥t cáº£ cÆ° d&amp;acirc;n Ä‘Æ°á»£c trao táº·ng nhá»¯ng Ä‘áº·c quyá»n ri&amp;ecirc;ng biá»‡t chÆ°a tá»«ng c&amp;oacute;.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Vinhomes Green Villas Äáº¡i Má»—&lt;/strong&gt;&amp;nbsp;c&amp;oacute; Ä‘áº§y Ä‘á»§ nhá»¯ng tiá»‡n &amp;iacute;ch hiá»‡n Ä‘áº¡i v&amp;agrave; Ä‘áº³ng cáº¥p báº­c nháº¥t, song váº«n táº­n dá»¥ng tá»‘i Ä‘a n&amp;eacute;t Ä‘á»™c Ä‘&amp;aacute;o cá»§a thi&amp;ecirc;n nhi&amp;ecirc;n Ä‘á»ƒ mang Ä‘áº¿n má»™t miá»n xanh tÆ°Æ¡i m&amp;aacute;t, Ä‘á»ƒ má»—i ng&amp;agrave;y táº¡i Ä‘&amp;acirc;y l&amp;agrave; má»™t ng&amp;agrave;y hÆ°á»Ÿng thá»¥ gi&amp;aacute; trá»‹ sá»‘ng Ä‘á»‰nh cao.&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n	&lt;ul&gt;\r\n		&lt;li&gt;T&amp;ecirc;n dá»± &amp;aacute;n:&amp;nbsp;&lt;strong&gt;Vinhomes Green Villas&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Chá»§ Ä‘áº§u tÆ°: Táº­p Ä‘o&amp;agrave;n Vingroup&lt;/li&gt;\r\n		&lt;li&gt;ÄÆ¡n vá»‹ quáº£n l&amp;yacute; v&amp;agrave; váº­n h&amp;agrave;nh: Vinhomes&lt;/li&gt;\r\n		&lt;li&gt;Vá»‹ tr&amp;iacute; dá»± &amp;aacute;n: PhÆ°á»ng Äáº¡i Má»—, Q. Nam Tá»« Li&amp;ecirc;m, H&amp;agrave; Ná»™i&lt;/li&gt;\r\n		&lt;li&gt;Tá»•ng diá»‡n t&amp;iacute;ch dá»± &amp;aacute;n:&amp;nbsp;&lt;strong&gt;13,7ha&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Äáº¥t x&amp;acirc;y dá»±ng biá»‡t thá»±:&amp;nbsp;&lt;strong&gt;2.800m2&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Sá»‘ lÆ°á»£ng biá»‡t thá»±:&amp;nbsp;&lt;strong&gt;98 cÄƒn&lt;/strong&gt;, 03 Shophouse&lt;/li&gt;\r\n		&lt;li&gt;Loáº¡i h&amp;igrave;nh biá»‡t thá»±:&amp;nbsp;&lt;strong&gt;Biá»‡t thá»± Ä‘Æ¡n láº­p&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Máº­t Ä‘á»™ x&amp;acirc;y dá»±ng:&lt;strong&gt;&amp;nbsp;chá»‰ 30%&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Äáº¥t c&amp;acirc;y xanh, máº·t há»“:&amp;nbsp;&lt;strong&gt;4,2ha&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Bá»ƒ bÆ¡i trong nh&amp;agrave; c&amp;oacute; m&amp;aacute;i che:&amp;nbsp;&lt;strong&gt;575m2&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Khu Ä‘á»— xe:&amp;nbsp;&lt;strong&gt;1,3ha&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;H&amp;igrave;nh thá»©c sá»Ÿ há»¯u:&amp;nbsp;&lt;strong&gt;Sá»• Ä‘á»&lt;/strong&gt;&amp;nbsp;kh&amp;ocirc;ng thá»i háº¡n&lt;/li&gt;\r\n	&lt;/ul&gt;\r\n	&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', 6, 20, 'lib_1.png', 'anh4.jpg', 'lib_3.png', '2019/06/21 02:56:40', 'Báº¥t Ä‘á»™ng sáº£n nghá»‰ dÆ°á»¡ng'),
(8, 'vinhomes-greenvillas', '12349977334', '&lt;p&gt;&lt;a href=&quot;http://bdscaocap.vn/&quot;&gt;&lt;strong&gt;Vinhomes Green Villas&lt;/strong&gt;&lt;/a&gt;&amp;nbsp;&amp;ndash; l&amp;agrave; quáº§n thá»ƒ&amp;nbsp;&lt;strong&gt;biá»‡t thá»± Ä‘Æ¡n láº­p&lt;/strong&gt;&amp;nbsp;kh&amp;eacute;p k&amp;iacute;n, sang trá»ng Ä‘em láº¡i cuá»™c sá»‘ng Ä‘á»‰nh cao ri&amp;ecirc;ng tÆ° cho nhá»¯ng kh&amp;aacute;ch h&amp;agrave;ng tinh hoa trong x&amp;atilde; há»™i.thi&amp;ecirc;n Ä‘Æ°á»ng sá»‘ng Ä‘&amp;iacute;ch thá»±c, nÆ¡i táº¥t cáº£ cÆ° d&amp;acirc;n Ä‘Æ°á»£c trao táº·ng nhá»¯ng Ä‘áº·c quyá»n ri&amp;ecirc;ng biá»‡t chÆ°a tá»«ng c&amp;oacute;.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Vinhomes Green Villas Äáº¡i Má»—&lt;/strong&gt;&amp;nbsp;c&amp;oacute; Ä‘áº§y Ä‘á»§ nhá»¯ng tiá»‡n &amp;iacute;ch hiá»‡n Ä‘áº¡i v&amp;agrave; Ä‘áº³ng cáº¥p báº­c nháº¥t, song váº«n táº­n dá»¥ng tá»‘i Ä‘a n&amp;eacute;t Ä‘á»™c Ä‘&amp;aacute;o cá»§a thi&amp;ecirc;n nhi&amp;ecirc;n Ä‘á»ƒ mang Ä‘áº¿n má»™t miá»n xanh tÆ°Æ¡i m&amp;aacute;t, Ä‘á»ƒ má»—i ng&amp;agrave;y táº¡i Ä‘&amp;acirc;y l&amp;agrave; má»™t ng&amp;agrave;y hÆ°á»Ÿng thá»¥ gi&amp;aacute; trá»‹ sá»‘ng Ä‘á»‰nh cao.&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;\r\n	&lt;ul&gt;\r\n		&lt;li&gt;T&amp;ecirc;n dá»± &amp;aacute;n:&amp;nbsp;&lt;strong&gt;Vinhomes Green Villas&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Chá»§ Ä‘áº§u tÆ°: Táº­p Ä‘o&amp;agrave;n Vingroup&lt;/li&gt;\r\n		&lt;li&gt;ÄÆ¡n vá»‹ quáº£n l&amp;yacute; v&amp;agrave; váº­n h&amp;agrave;nh: Vinhomes&lt;/li&gt;\r\n		&lt;li&gt;Vá»‹ tr&amp;iacute; dá»± &amp;aacute;n: PhÆ°á»ng Äáº¡i Má»—, Q. Nam Tá»« Li&amp;ecirc;m, H&amp;agrave; Ná»™i&lt;/li&gt;\r\n		&lt;li&gt;Tá»•ng diá»‡n t&amp;iacute;ch dá»± &amp;aacute;n:&amp;nbsp;&lt;strong&gt;13,7ha&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Äáº¥t x&amp;acirc;y dá»±ng biá»‡t thá»±:&amp;nbsp;&lt;strong&gt;2.800m2&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Sá»‘ lÆ°á»£ng biá»‡t thá»±:&amp;nbsp;&lt;strong&gt;98 cÄƒn&lt;/strong&gt;, 03 Shophouse&lt;/li&gt;\r\n		&lt;li&gt;Loáº¡i h&amp;igrave;nh biá»‡t thá»±:&amp;nbsp;&lt;strong&gt;Biá»‡t thá»± Ä‘Æ¡n láº­p&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Máº­t Ä‘á»™ x&amp;acirc;y dá»±ng:&lt;strong&gt;&amp;nbsp;chá»‰ 30%&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Äáº¥t c&amp;acirc;y xanh, máº·t há»“:&amp;nbsp;&lt;strong&gt;4,2ha&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Bá»ƒ bÆ¡i trong nh&amp;agrave; c&amp;oacute; m&amp;aacute;i che:&amp;nbsp;&lt;strong&gt;575m2&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;Khu Ä‘á»— xe:&amp;nbsp;&lt;strong&gt;1,3ha&lt;/strong&gt;&lt;/li&gt;\r\n		&lt;li&gt;H&amp;igrave;nh thá»©c sá»Ÿ há»¯u:&amp;nbsp;&lt;strong&gt;Sá»• Ä‘á»&lt;/strong&gt;&amp;nbsp;kh&amp;ocirc;ng thá»i háº¡n&lt;/li&gt;\r\n	&lt;/ul&gt;\r\n	&lt;/li&gt;\r\n&lt;/ul&gt;\r\n', 500, 98, 'a7.jpg', '', '', '2019/06/21 01:25:00', 'Báº¥t Ä‘á»™ng sáº£n nghá»‰ dÆ°á»¡ng'),
(9, 'Biá»‡t thá»± Vinhomes Riverside', '1234997733444', '&lt;p&gt;&lt;strong&gt;Khu biá»‡t thá»±&amp;nbsp;Vinhomes Riverside&lt;/strong&gt;&amp;nbsp;thiáº¿t káº¿ v&amp;agrave; x&amp;acirc;y dá»±ng theo m&amp;ocirc; h&amp;igrave;nh cá»§a th&amp;agrave;nh phá»‘ Venice (Italy), má»™t th&amp;agrave;nh phá»‘ xinh Ä‘áº¹p, sang trá»ng v&amp;agrave; quyáº¿n rÅ©. ÄÆ°á»£c&amp;nbsp;káº¿t ná»‘i th&amp;ocirc;ng minh giá»¯a c&amp;aacute;c cÄƒn biá»‡t thá»± b&amp;ecirc;n s&amp;ocirc;ng Ä‘áº³ng cáº¥p, thiáº¿t káº¿ theo phong c&amp;aacute;ch T&amp;acirc;n cá»• Ä‘iá»ƒn l&amp;atilde;ng máº¡n, hiá»‡n Ä‘áº¡i bao gá»“m: Há»‡ thá»‘ng vÄƒn ph&amp;ograve;ng háº¡ng sang,&amp;nbsp;Trung t&amp;acirc;m thÆ°Æ¡ng máº¡i cao cáº¥p; Khu vui chÆ¡i giáº£i tr&amp;iacute; khá»•ng lá»“,&amp;nbsp;Khu áº©m thá»±c phong ph&amp;uacute;,&amp;nbsp;TrÆ°á»ng phá»• th&amp;ocirc;ng v&amp;agrave; TrÆ°á»ng máº§m non,&amp;nbsp;Chuá»—i c&amp;ocirc;ng vi&amp;ecirc;n c&amp;acirc;y xanh v&amp;agrave; há»“ nÆ°á»›c rá»™ng gáº§n 600.000 m2.&lt;/p&gt;\r\n\r\n&lt;p&gt;Táº¥t cáº£ c&amp;aacute;c&amp;nbsp;&lt;em&gt;biá»‡t thá»±&lt;/em&gt;&amp;nbsp;táº¡i Khu&amp;nbsp;&lt;em&gt;Ä‘&amp;ocirc; thá»‹ sinh th&amp;aacute;i Vinhomes Riverside&lt;/em&gt;&amp;nbsp;Ä‘á»u mang phong c&amp;aacute;ch t&amp;acirc;n tiáº¿n, phá»‘i káº¿t há»£p h&amp;agrave;i h&amp;ograve;a giá»¯a thiáº¿t káº¿ Ch&amp;acirc;u &amp;Acirc;u sang trá»ng, Ä‘áº³ng cáº¥p v&amp;agrave;&amp;nbsp;phong c&amp;aacute;ch thiáº¿t káº¿ Ch&amp;acirc;u &amp;Aacute; th&amp;acirc;n thiá»‡n, áº¥m c&amp;ugrave;ng táº¡o ra sá»± &amp;ldquo;kh&amp;ocirc;ng giá»‘ng ai&amp;rdquo; táº¡i&amp;nbsp;&lt;em&gt;&lt;strong&gt;biá»‡t thá»± Vinhomes Riverside&lt;/strong&gt;&lt;/em&gt;.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;Sang trá»ng, Ä‘áº³ng cáº¥p Ä‘&amp;oacute; l&amp;agrave; nhá»¯ng yáº¿u tá»‘ táº¡o n&amp;ecirc;n 5 khu biá»‡t thá»± :&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;ul&gt;\r\n	&lt;li&gt;&lt;strong&gt;&lt;em&gt;Biá»‡t thá»±&amp;nbsp;Hoa Lan&lt;/em&gt;&lt;/strong&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;strong&gt;&lt;em&gt;Biá»‡t thá»±&amp;nbsp;Hoa Sá»¯a&lt;/em&gt;&lt;/strong&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;a href=&quot;http://tanthoidai.com.vn/ban-biet-thu-hoa-anh-dao-vinhomes-riverside-5051.html&quot;&gt;&lt;strong&gt;&lt;em&gt;Biá»‡t thá»±&amp;nbsp;&amp;nbsp;Anh Ä&amp;agrave;o&lt;/em&gt;&lt;/strong&gt;&lt;/a&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;strong&gt;&lt;em&gt;Biá»‡t thá»±&amp;nbsp;Hoa PhÆ°á»£ng&lt;/em&gt;&lt;/strong&gt;&lt;/li&gt;\r\n	&lt;li&gt;&lt;strong&gt;&lt;em&gt;Biá»‡t thá»±&amp;nbsp;Báº±ng lÄƒng&lt;/em&gt;&lt;/strong&gt;&lt;/li&gt;\r\n&lt;/ul&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;&lt;/p&gt;\r\n', 5000000000, 5, 'a54.JPG', '', '', '2019/06/21 01:30:46', 'Báº¥t Ä‘á»™ng sáº£n nghá»‰ dÆ°á»¡ng'),
(10, 'Vincity Sportia', '123499773345667', '&lt;p&gt;Chá»§ Ä‘áº§u tÆ° Vingroup ch&amp;iacute;nh thá»©c má»Ÿ b&amp;aacute;n tá»• há»£p dá»± &amp;aacute;n Vinhomes Smart City T&amp;acirc;y Má»— - Äáº¡i Má»— (Vincity Sportia cÅ©) lá»›n nháº¥t ph&amp;iacute;a T&amp;acirc;y thá»§ Ä‘&amp;ocirc; vá»›i 58 t&amp;ograve;a chung cÆ° gá»“m h&amp;agrave;ng chá»¥c ngh&amp;igrave;n cÄƒn há»™ cao cáº¥p,&amp;nbsp;&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;nbsp;Vinhomes Sapphire, Vinhomes Ruby, Vinhomes Diamond&lt;/p&gt;\r\n\r\n&lt;p&gt;c&amp;oacute; 5 ph&amp;acirc;n khu&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;ndash; The Hero: 9 t&amp;ograve;a&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;ndash; The Dream: 11 t&amp;ograve;a&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;ndash; The Power: 17 t&amp;ograve;a&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;ndash; The Victory: 12 t&amp;ograve;a&lt;/p&gt;\r\n\r\n&lt;p&gt;&amp;ndash; The Sun: 9 t&amp;ograve;a&lt;/p&gt;\r\n', 200000000, 58, 'a55.jpg', '', '', '2019/06/21 01:35:12', 'Báº¥t Ä‘á»™ng sáº£n nghá»‰ dÆ°á»¡ng'),
(11, 'Vinhomes Ocean Park', '123499773345667', '&lt;p&gt;Kh&amp;ocirc;ng chá»‰ l&amp;agrave; má»™t Äáº¡i Ä‘&amp;ocirc; thá»‹ Ä‘áº³ng cáº¥p quá»‘c táº¿,&amp;nbsp;&lt;strong&gt;Vinhomes Ocean Park&lt;/strong&gt;&amp;nbsp;kiáº¿n táº¡o n&amp;ecirc;n má»™t Th&amp;agrave;nh phá»‘ má»›i vá»›i Thi&amp;ecirc;n nhi&amp;ecirc;n &amp;ndash; Cuá»™c sá»‘ng v&amp;agrave; Con ngÆ°á»i vá»›i má»™t diá»‡n máº¡o má»›i máº» v&amp;agrave; tinh tháº§n há»©ng khá»Ÿi, sáºµn s&amp;agrave;ng cho nhá»¯ng tráº£i nghiá»‡m tÆ°á»Ÿng kh&amp;ocirc;ng thá»ƒ m&amp;agrave; láº¡i l&amp;agrave; c&amp;oacute; thá»ƒ.&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;em&gt;NÆ¡i liá»n ká» phá»‘ phÆ°á»ng s&amp;ocirc;i Ä‘á»™ng l&amp;agrave; xanh ng&amp;aacute;t Ä‘áº¡i dÆ°Æ¡ng&lt;br /&gt;\r\nNÆ¡i giá»¯a ná»™i Ä‘&amp;ocirc; l&amp;agrave; cáº­n biá»ƒn ká» há»“, l&amp;agrave; g&amp;oacute;c bá»ƒ ch&amp;acirc;n m&amp;acirc;y&lt;br /&gt;\r\nNÆ¡i chá»‰ má»™t bÆ°á»›c l&amp;agrave; thÆ° th&amp;aacute;i v&amp;ugrave;i ch&amp;acirc;n dÆ°á»›i l&amp;agrave;n c&amp;aacute;t má»‹n v&amp;agrave; cÅ©ng chá»‰ má»™t bÆ°á»›c l&amp;agrave; thá»a sá»©c reo vui vá»›i nhá»‹p sá»‘ng nÄƒng Ä‘á»™ng tá»«ng ng&amp;agrave;y.&lt;/em&gt;&lt;/p&gt;\r\n\r\n&lt;p&gt;&lt;strong&gt;Vinhomes Ocean Park&lt;/strong&gt;&amp;nbsp;&amp;ndash; Th&amp;agrave;nh phá»‘ Biá»ƒn há»“ &amp;ndash; NÆ¡i biáº¿n nhá»¯ng Ä‘iá»u kh&amp;ocirc;ng thá»ƒ th&amp;agrave;nh c&amp;oacute; thá»ƒ.&lt;/p&gt;\r\n', 600000000, 0, 'a56.jpg', '', '', '2019/06/21 01:38:41', 'Báº¥t Ä‘á»™ng sáº£n nghá»‰ dÆ°á»¡ng');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `tblslideshow`
--
ALTER TABLE `tblslideshow`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_baiviet`
--
ALTER TABLE `tbl_baiviet`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_chitiethoadon`
--
ALTER TABLE `tbl_chitiethoadon`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_diachiduan`
--
ALTER TABLE `tbl_diachiduan`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_diachigiaodich`
--
ALTER TABLE `tbl_diachigiaodich`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_gioithieu`
--
ALTER TABLE `tbl_gioithieu`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_hoadon`
--
ALTER TABLE `tbl_hoadon`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_khachhang`
--
ALTER TABLE `tbl_khachhang`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_nguoidung`
--
ALTER TABLE `tbl_nguoidung`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tbl_sanpham`
--
ALTER TABLE `tbl_sanpham`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `tblslideshow`
--
ALTER TABLE `tblslideshow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_baiviet`
--
ALTER TABLE `tbl_baiviet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `tbl_chitiethoadon`
--
ALTER TABLE `tbl_chitiethoadon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `tbl_diachiduan`
--
ALTER TABLE `tbl_diachiduan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_diachigiaodich`
--
ALTER TABLE `tbl_diachigiaodich`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_gioithieu`
--
ALTER TABLE `tbl_gioithieu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `tbl_hoadon`
--
ALTER TABLE `tbl_hoadon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tbl_khachhang`
--
ALTER TABLE `tbl_khachhang`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `tbl_nguoidung`
--
ALTER TABLE `tbl_nguoidung`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `tbl_sanpham`
--
ALTER TABLE `tbl_sanpham`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
